# Description
A replacement for KShortcutsDialog using Kirigami/FormCard. 

This provides a simple component (ShortcutView) that will allow the user to choose the default shortcut for an action or set a custom one.

![](ShortcutView.png)

## How to use ShortcutView:
Put the ShortcutView inside a Page, a Dialog, or where ever you like (it's just a ColumnLayout) and provides some info about the actions.

The `actions` should be an `object` where the keys are the name of the collection of actions (General, Text actions, ...) and the values are `list` of `object` providing the minimum amount of info about the action:
- name (required): A unique name to save the action and be able to later call it
- shortcut (required): The default shortcut for this action
- text (optionnal): The action text (if not provided, will fallback to name)

Exemple (from the app):
```
{
    "Collection 1": [
        {
            "name": "action1",
            "shortcut": "Ctrl+1",
            "text": i18n("Action 1 triggered")
        },
        {
            "name": "action2",
            "shortcut": "Ctrl+2",
            "text": i18n("Action 2 triggered")
        },
    ],
    "Collection 2": [
        {
            "name": "action3",
            "shortcut": "Ctrl+3",
            "text": i18n("Action 3 triggered")
        },
        {
            "name": "action4",
            "shortcut": "Ctrl+4",
            "text": i18n("Action 4 triggered")
        },
    ]
}

```

Those actions can be passed to the ShortcutView or loaded from elsewhere using `ActionHandler.actions = <actions object>`, this way, the ActionHandler can be setup without the need to load the page containing the ShortcutView.

#### Why not more ?
This is the strict minimum to be able to easily manage the different actions/shortcuts, no more, no less, all the rest should be provided directly inside the QML action itself.

#### Why not use QML action inside the list ?
This might be possible, but I have not figured out a way to convert a QML action and use its property in C++, this is the plan B

## Reuse actions from the ShortcutView:

Use a KActionFromAction and give it the `actionName` of the wanted action (`main.qml` line 87):
```
KActionFromAction {
    actionName: "action1"
    visible: false

    onTriggered: {
        actionLabel.text = text 
    }
}
```

KActionFromAction is just a Kirigami.Action wrapper that will automaticly fill the given info (text and shortcut) from the provided QACtion.

This means that the rest of the property can be set as usual.

## TODO 
- [ ] Make sure that this works well with Dialog (not sure about `FormKeySequenceItem` line 189)
- [ ] Figure out why the window looses all focus after entering a custom shortcut
- [x] Figure out a way to "setupActions" even when the `ShortcutView` is not directly loaded
