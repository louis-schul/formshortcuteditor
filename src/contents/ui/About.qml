// SPDX-License-Identifier: GPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import org.kde.kirigamiaddons.formcard 1.0 as FormCard

FormCard.AboutPage {
    aboutData: About
}
