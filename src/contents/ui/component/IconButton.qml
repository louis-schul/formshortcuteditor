// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

/*
 * BASED on FormCardButtonDelegate :
 * Copyright 2022 Devin Lin <devin@kde.org>
 * SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0

AbstractFormDelegate {
    id: root

    focusPolicy: Qt.StrongFocus

    contentItem: RowLayout {
        spacing: 0

        Kirigami.Icon {
            id: icon
            visible: root.icon.name !== ""
            source: root.icon.name
            color: root.icon.color
            Layout.rightMargin: (root.icon.name !== "" && root.text.length > 0)
                ? Kirigami.Units.largeSpacing + Kirigami.Units.smallSpacing
                : 0
            Layout.alignment: (root.text.length > 0) ? Qt.AlignLeft : Qt.AlignHCenter
            implicitWidth: (root.icon.name !== "") ? Kirigami.Units.iconSizes.small : 0
            implicitHeight: (root.icon.name !== "") ? Kirigami.Units.iconSizes.small : 0
        }

        Label {
            Layout.fillWidth: root.text.length > 0
            text: root.text
            elide: Text.ElideRight
            wrapMode: Text.Wrap
            maximumLineCount: 2
            color: root.enabled ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
            Accessible.ignored: true // base class sets this text on root already
        }
    }

    Accessible.onPressAction: action ? action.trigger() : root.clicked()
}

