// SPDX-FileCopyrightText: 2021 Claudio Cambra <claudio.cambra@gmail.com>
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick 2.0 
import org.kde.kirigami 2.20 as Kirigami

import org.kde.formshortcuteditor 1.0

Kirigami.Action {
    required property string actionName
    readonly property var action: ActionHandler.action(actionName)

    text: action ? action.text : ""
    shortcut: action ? action.shortcut : ""
}
