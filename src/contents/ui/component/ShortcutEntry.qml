// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.7
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

import org.kde.formshortcuteditor 1.0

ColumnLayout {
    id: root

    //Can't make those required property, this will messed up with the index :/
    property alias action: mainHolder.action
    property alias actionIndex: mainHolder.actionIndex
    property alias collecIndex: mainHolder.collecIndex
    property bool isExtended: false

    spacing: 0

    FormCard.AbstractFormDelegate {
        focusPolicy: Qt.StrongFocus

        contentItem: RowLayout {
            Kirigami.Heading {
                text: root.action.text
                level: 2

                Layout.fillWidth: true
            }

            FormCard.FormArrow {
                Layout.alignment: Qt.AlignRight| Qt.AlignVCenter
                direction: root.isExtended ? Qt.UpArrow : Qt.DownArrow
            }

            Layout.preferredWidth: Kirigami.Units.gridUnit

        }
        Accessible.onPressAction: clicked()
        onClicked: root.isExtended = !root.isExtended
    }

    FormKeySequenceItem {
        id: mainHolder

        Layout.topMargin: opacity > 0 ? Kirigami.Units.smallSpacing : 0
        Layout.preferredHeight: Kirigami.Units.gridUnit * 4 + Kirigami.Units.largeSpacing + Kirigami.Units.smallSpacing

        states: State {
            name: "invisible"
            when: !root.isExtended
            PropertyChanges { target: mainHolder; Layout.preferredHeight: 0 }
            PropertyChanges { target: mainHolder; opacity: 0 }
        }

        Behavior on Layout.preferredHeight {
            NumberAnimation { duration: Kirigami.Units.shortDuration }
        }
        Behavior on opacity {
            NumberAnimation { duration: Kirigami.Units.veryShortDuration}
        }

        onKeySequenceModified: ActionHandler.changeShortcut(collecIndex, actionIndex, keySequence)
    }
}
