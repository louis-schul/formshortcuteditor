// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

import org.kde.formshortcuteditor 1.0

ColumnLayout {
    id: root

    property var actions
    
    Component.onCompleted: {
        if (!ActionHandler.hasActions) {
            ActionHandler.actions = actions
        }
    }

    Repeater {
        id: repeater
        Layout.fillWidth: true
        Layout.fillHeight: true

        delegate: ColumnLayout {
            property int collectionIndex: index

            FormCard.FormHeader {
                id: head
                title: ActionHandler.collectionName(collectionIndex)
                Layout.fillWidth : true
            }

            FormCard.FormCard {
                Layout.fillWidth : true
                Repeater {
                    model: ActionHandler.actionsCount(collectionIndex)

                    ShortcutEntry {
                        actionIndex: index
                        collecIndex: collectionIndex
                        action: ActionHandler.getActionInfo(collecIndex, actionIndex)
                    }
                }
            }
        }
    }

    Connections {
        target: ActionHandler

        function onActionsAdded() {
            repeater.model = ActionHandler.collectionCount()
        }
    }
}
