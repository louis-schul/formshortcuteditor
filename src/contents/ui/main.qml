// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

import org.kde.formshortcuteditor 1.0

Kirigami.ApplicationWindow {
    id: root

    title: i18n("formShortCutEditor")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    onClosing: App.saveWindowGeometry(root)

    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()
    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()

    readonly property var shortcutActions: {
        "Collection 1": [
            {
                "name": "action1",
                "shortcut": "Ctrl+1",
                "text": i18n("Action 1")
            },
            {
                "name": "action2",
                "shortcut": "Ctrl+2",
                "text": i18n("Action 2")
            },
        ],
        "Collection 2": [
            {
                "name": "action3",
                "shortcut": "Ctrl+3",
                "text": i18n("Action 3")
            },
            {
                "name": "action4",
                "shortcut": "Ctrl+4",
                "text": i18n("Action 4")
            },
        ]
    }

    Component.onCompleted: {
        App.restoreWindowGeometry(root)

        ActionHandler.actions = shortcutActions;
        pageStack.push(shortcutPage)
    }

    // This timer allows to batch update the window size change to reduce
    // the io load and also work around the fact that x/y/width/height are
    // changed when loading the page and overwrite the saved geometry from
    // the previous session.
    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    pageStack.initialPage:actionPage 

    FormCard.FormCardPage {
        id: shortcutPage

        title: i18n("Main Page")

        ShortcutView {
            id: shortcutView

            actions: shortcutActions
        }
    }
    
    FormCard.FormCardPage {
        id: actionPage

        title: "Action demo"
        actions.contextualActions: [
            KActionFromAction {
                actionName: "action1"
                visible: false

                onTriggered: {
                    actionLabel.text = text + " triggered"
                }
            },
            KActionFromAction {
                actionName: "action2"
                visible: false

                onTriggered: {
                    actionLabel.text = text + " triggered"
                }
            },
            KActionFromAction {
                actionName: "action3"
                visible: false

                onTriggered: {
                    actionLabel.text = text + " triggered"
                }
            },
            KActionFromAction {
                actionName: "action4"
                visible: false

               onTriggered: {
                    actionLabel.text = text + " triggered"
                }
            }
        ]

        Kirigami.Heading {
            text: "Try the shortcut for a given action and see if it works:"
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap

            Layout.fillWidth: true
        }
 
        Controls.Label {
            id: actionLabel

            wrapMode: Text.WordWrap

            Layout.fillWidth: true
            Layout.margins: Kirigami.Units.largeSpacing
        }
    }
}
