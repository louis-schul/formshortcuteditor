// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

#include "actionHandler.h"

#include <KAuthorized>
#include <KLocalizedString>
#include <KSharedConfig>
#include <QDebug>

QList<QKeySequence> shortcutsFromString(const QStringList &strings)
{
    QList<QKeySequence> shortcuts;

    for (const auto &string : strings) {
        const QKeySequence shortcut(string);
        shortcuts.append(shortcut);
    }
    return shortcuts;
}

QList<QString> shortcutsToString(const QList<QKeySequence> &shortcuts)
{
    QStringList shortcutsStrList;

    for (const auto &shortcut : shortcuts) {
        shortcutsStrList.append(shortcut.toString());
    }

    return shortcutsStrList;
}

ActionHandler::ActionHandler(QObject *parent)
    : QObject(parent)
{
    m_shortcutConfig = new KConfigGroup(KSharedConfig::openConfig(), "Shortcuts");
}

ActionHandler::~ActionHandler() noexcept
{
}

bool ActionHandler::hasActions() const
{
    return m_hasActions;
}

void ActionHandler::saveShortcuts(const QString &actionName, const QList<QKeySequence> &shortcuts) const
{
    QStringList shortcutsStrList = shortcutsToString(shortcuts);

    m_shortcutConfig->writeEntry(actionName, shortcutsStrList);
}

void ActionHandler::addActions(const QVariantMap &actions)
{
    for (const auto &collectionName : actions.keys()) {
        const QVariantList collection = actions[collectionName].toList();
        QList<QAction *> actionsList;

        for (auto it = collection.begin(); it != collection.end(); it++) {
            const QVariantMap actionInfo = it->toMap();

            const QVariant nameInfo = actionInfo[QStringLiteral("name")];
            if (!nameInfo.isValid() || !nameInfo.canConvert<QString>()) {
                continue;
            }

            const QVariant shortcutInfo = actionInfo[QStringLiteral("shortcut")];
            if (!shortcutInfo.isValid() || !shortcutInfo.canConvert<QString>()) {
                continue;
            }

            const QVariant textInfo = actionInfo[QStringLiteral("text")];
            const bool validText = textInfo.isValid() && textInfo.canConvert<QString>();

            const QString actionName = nameInfo.toString();
            const QKeySequence actionDefaultShortcut(shortcutInfo.toString());
            const QString actionText = validText ? textInfo.toString() : actionName;

            if (KAuthorized::authorizeAction(actionName)) {
                QAction *action = new QAction; // Can't be const, setter won't work

                const QStringList savedShortcutsStr = m_shortcutConfig->readEntry(actionName).split(QStringLiteral(","));
                const QList<QKeySequence> savedShortcuts = shortcutsFromString(savedShortcutsStr);

                QList<QKeySequence> actionShortcuts = {actionDefaultShortcut};
                if (!savedShortcuts.isEmpty()) {
                    actionShortcuts = savedShortcuts;
                }
                action->setObjectName(actionName);
                action->setProperty("defaultShortcut", QVariant::fromValue(actionDefaultShortcut));
                action->setShortcuts(actionShortcuts);
                action->setText(actionText);
                actionsList.append(action);
            }
        }

        if (!actionsList.isEmpty()) {
            m_collections[collectionName] = actionsList;
            m_collectionsNames.append(collectionName);
        }
        Q_EMIT actionsAdded();
        m_hasActions = true;
    }
}

int ActionHandler::collectionCount() const
{
    return m_collections.size();
}

QString ActionHandler::collectionName(const int collectionIndex) const
{
    return m_collectionsNames[collectionIndex];
}

QList<QAction *> ActionHandler::collectionAt(const int collectionIndex) const
{
    return m_collections[collectionName(collectionIndex)];
}

QAction *ActionHandler::action(const QString &name)
{
    if (!m_hasActions) {
        return {};
    }

    const auto collections = m_collections;
    for (const auto &collection : collections) {
        for (const auto &action : collection) {
            if (action->objectName() == name) {
                return action;
            }
        }
    }

    qWarning() << "Not found action for name" << name;

    return {};
}

int ActionHandler::actionsCount(const int collectionIndex) const
{
    return collectionAt(collectionIndex).count();
}

QKeySequence ActionHandler::actionDefaultShortcut(const QAction *action) const
{
    return qvariant_cast<QKeySequence>(action->property("defaultShortcut"));
}

QKeySequence ActionHandler::actionCustomShortcut(const QAction *action) const
{
    auto shortcuts = action->shortcuts();
    if (shortcuts.isEmpty())
        return QKeySequence();

    QKeySequence defaultShortcut = actionDefaultShortcut(action);
    QKeySequence currentShortcut = shortcuts[0];
    // need to convert to string to check if the 'KeySequence' is not a simple ""
    if (currentShortcut != defaultShortcut && !currentShortcut.toString().isEmpty()) {
        return currentShortcut;
    }
    if (shortcuts.size() > 1 && shortcuts[1] != defaultShortcut) {
        return shortcuts[1];
    }

    return QKeySequence();
}

void ActionHandler::changeShortcut(QAction *action, const QKeySequence &sequence, const bool save)
{
    QKeySequence defaultShortcut = actionDefaultShortcut(action);
    QKeySequence currentShortcut = action->shortcut();
    QList<QKeySequence> shortcuts;
    // User has selected the default, but has a custom shortcut (we still need to save the custom)
    if (sequence == defaultShortcut && currentShortcut != defaultShortcut) {
        shortcuts = {defaultShortcut, currentShortcut};
    } else {
        shortcuts = {sequence}; // We have the default, the only needed info is the custom (selected)
    }

    action->setShortcuts(shortcuts);

    if (save) {
        saveShortcuts(action->objectName(), shortcuts);
    }
}

void ActionHandler::changeShortcut(const int collectionIndex, const int actionIndex, const QKeySequence &sequence)
{
    QAction *action = actionAt(collectionIndex, actionIndex);

    changeShortcut(action, sequence);
}

QAction *ActionHandler::actionAt(const int collectionIndex, const int actionIndex) const
{
    QList<QAction *> collection = collectionAt(collectionIndex);
    if (actionIndex >= 0 && actionIndex < collection.size()) {
        return collection.at(actionIndex);
    }
    return nullptr;
}

QVariantMap ActionHandler::getActionInfo(QAction *action)
{
    if (!action) {
        return {};
    }

    QKeySequence defaultShortcut = actionDefaultShortcut(action);
    QKeySequence customShortcut = actionCustomShortcut(action);

    QKeySequence actualShortcut = action->shortcut();
    if (actualShortcut.toString().isEmpty()) {
        actualShortcut = defaultShortcut;
        changeShortcut(action, defaultShortcut);
    }

    QString actionText = action->text();

    // The rest can directly be applied in qml
    QVariantMap returnMap = {
        {QStringLiteral("defaultSequence"), QVariant(defaultShortcut)},
        {QStringLiteral("customSequence"), QVariant(customShortcut)},
        {QStringLiteral("shortcut"), QVariant(actualShortcut)},
        {QStringLiteral("text"), QVariant(actionText)},
    };

    return returnMap;
}

/*
 * This whole method is needed to get a valid shortcut (and not a "")
 * without the need to check + clean every action
 * It also gather all the required info into a single object/single function call in QML
 */
QVariantMap ActionHandler::getActionInfo(const int collectionIndex, const int actionIndex)
{
    QAction *action = actionAt(collectionIndex, actionIndex);

    return getActionInfo(action);
}
