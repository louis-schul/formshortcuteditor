// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

#pragma once

#include <KConfigGroup>
#include <QAction>
#include <QObject>

class ActionHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap actions WRITE addActions)
    Q_PROPERTY(bool hasActions READ hasActions CONSTANT)
public:
    explicit ActionHandler(QObject *parent = nullptr);
    ~ActionHandler();

    Q_INVOKABLE QAction *action(const QString &actionName);
    Q_INVOKABLE int collectionCount() const;
    Q_INVOKABLE QString collectionName(const int collectionIndex) const;
    Q_INVOKABLE int actionsCount(const int collectionIndex) const;
    Q_INVOKABLE void changeShortcut(const int collectionIndex, const int actionIndex, const QKeySequence &sequence);
    Q_INVOKABLE QVariantMap getActionInfo(const int collectionIndex, const int actionIndex);
    Q_INVOKABLE bool hasActions() const;

Q_SIGNALS:
    void actionsAdded();

private:
    void addActions(const QVariantMap &actions);
    QAction *actionAt(const int collectionIndex, const int actionIndex) const;
    QList<QAction *> collectionAt(const int collectionIndex) const;
    QKeySequence actionDefaultShortcut(const QAction *action) const;
    QKeySequence actionCustomShortcut(const QAction *action) const;
    QVariantMap getActionInfo(QAction *action);
    void changeShortcut(QAction *action, const QKeySequence &sequence, const bool save = true);
    void saveShortcuts(const QString &actionName, const QList<QKeySequence> &shortcuts) const;

    QMap<QString, QList<QAction *>> m_collections;
    QStringList m_collectionsNames; // Store the collection in the order the user sends them
    KConfigGroup *m_shortcutConfig = nullptr;
    bool m_hasActions = false;
};
