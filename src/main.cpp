// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QUrl>
#include <qqml.h>
#ifdef Q_OS_ANDROID
#include <GuiQApplication>
#else
#include <QApplication>
#endif

#include "app.h"
#include "version-formshortcuteditor.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "formshortcuteditorconfig.h"
#include "logic/actionHandler.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    // Default to org.kde.desktop style unless the user forces another style
    if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
        QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
    }

    KLocalizedString::setApplicationDomain("formshortcuteditor");
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));

    KAboutData aboutData(
        // The program name used internally.
        QStringLiteral("formshortcuteditor"),
        // A displayable program name string.
        i18nc("@title", "formShortCutEditor"),
        // The program version string.
        QStringLiteral(FORMSHORTCUTEDITOR_VERSION_STRING),
        // Short description of what the app does.
        i18n("Application Description"),
        // The license this code is released under.
        KAboutLicense::GPL,
        // Copyright Statement.
        i18n("(c) 2023"));
    aboutData.addAuthor(i18nc("@info:credit", "Louis Schul"),
                        i18nc("@info:credit", "Maintainer"),
                        QStringLiteral("schul9louis@gmail.com"),
                        QStringLiteral("https://yourwebsite.com"));
    aboutData.setTranslator(i18nc("NAME OF TRANSLATORS", "Your names"), i18nc("EMAIL OF TRANSLATORS", "Your emails"));
    KAboutData::setApplicationData(aboutData);
    QGuiApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("org.kde.formshortcuteditor")));

    QQmlApplicationEngine engine;

    // auto config = formShortCutEditorConfig::self();

    // qmlRegisterSingletonInstance("org.kde.formshortcuteditor", 1, 0, "Config", config);

    qmlRegisterSingletonType("org.kde.formshortcuteditor", 1, 0, "About", [](QQmlEngine *engine, QJSEngine *) -> QJSValue {
        return engine->toScriptValue(KAboutData::applicationData());
    });

    App application;
    qmlRegisterSingletonInstance("org.kde.formshortcuteditor", 1, 0, "App", &application);

    ActionHandler actionHandler;
    qmlRegisterSingletonInstance("org.kde.formshortcuteditor", 1, 0, "ActionHandler", &actionHandler);

    qRegisterMetaType<QAction *>();

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
